from features_extractors.feature_extractor import FeatureExtractor
from outputs.output_layer import OutputLayer
import logging


class Director:
    def __init__(self, output, fe_builder):
        self.__fe_builder = fe_builder   # type: FeatureExtractor
        self.__output     = output       # type: OutputLayer
        self.__logger     = logging.getLogger("director")
        self.__logger.setLevel(logging.DEBUG)
        self.__formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        self.__fh = logging.FileHandler('debug.log', mode='w')
        self.__fh.setLevel(logging.INFO)
        self.__fh.setFormatter(self.__formatter)
        self.__logger.addHandler(self.__fh)
        self.__logger.info("initialization completed")
        self.__fe_builder.set_logger(self.__logger)

    def extract_features(self):
        context = self.__fe_builder.initialize_context()
        try:
            file_based_features, sample_based_features = self.__fe_builder.get_lists_of_features()
            self.__output.setup(file_based_features_names=file_based_features, sample_based_features_names=sample_based_features)
            for sample_index, sample in enumerate(self.__fe_builder.get_samples()):
                self.__logger.info("extracting features for sample {0}".format(sample_index))

                features_list, features_dict = self.__fe_builder.extract_sample_based_features(sample, sample_index, context)

                if self.__output.output_format == "list":
                    self.__output.send(features_list, self.__logger)
                elif self.__output.output_format == "dict":
                    self.__output.send(features_dict, self.__logger)

            self.__output.setup_output_for_file_based_features(file_based_features_names=file_based_features)

            file_based_features = self.__fe_builder.extract_file_based_features()
            self.__output.send(file_based_features, self.__logger)

            self.__output.tear_down()

        except Exception as e:
            print(e.__class__, str(e))

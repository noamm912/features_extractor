import sys


class IntensityFeatures:
    def __init__(self):
        self.__min = sys.maxsize
        self.__min_x = -1
        self.__max = 0
        self.__max_x = -1
        self.__avg = 0
        self.__avg_x = -1
        self.__most_frequent = None
        self.__most_frequent_x = -1

        self.count = {}
        self.count_x = {}

    @property
    def min(self):
        return self.__min

    @min.setter
    def min(self, v):
        self.__min = v

    @property
    def max(self):
        return self.__max

    @max.setter
    def max(self, v):
        self.__max = v

    @property
    def avg(self):
        return self.__avg

    @avg.setter
    def avg(self, v):
        self.__avg = v

    @property
    def most_frequent(self):
        return self.__most_frequent

    @most_frequent.setter
    def most_frequent(self, v):
        self.__most_frequent = v

    @property
    def min_x(self):
        return self.__min_x

    @min_x.setter
    def min_x(self, v):
        self.__min_x = v

    @property
    def max_x(self):
        return self.__max_x

    @max_x.setter
    def max_x(self, v):
        self.__max_x = v

    @property
    def avg_x(self):
        return self.__avg_x

    @avg_x.setter
    def avg_x(self, v):
        self.__avg_x = v

    @property
    def most_frequent_x(self):
        return self.__most_frequent_x

    @most_frequent_x.setter
    def most_frequent_x(self, v):
        self.__most_frequent_x = v

    @staticmethod
    def pretty(attr):
        l = attr.split('_')
        l = [w.capitalize() for w in l]
        if l[-1] == 'X':
            l[-1] = 'Intensity Last X Notes'
        else:
            l.append("Intensity")
        return ' '.join(l)

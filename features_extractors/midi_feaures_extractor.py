from features_extractors.feature_extractor import FeatureExtractor
from collections import OrderedDict
import sys
import mido
import logging
from .intensity import IntensityFeatures
from .octave import OctaveFeatures
from .transitions import TransitionsFeatures

SAMPLE_BASED_FEATURES = ['Note',
                         'Intensity',
                         'Time',

                         'Lowest_intensity',
                         'Lowest_intensity_last_X_notes',
                         'Highest_intensity',
                         'Highest_intensity_last_X_notes',
                         'Average_intensity',
                         'Average_intensity_last_X_notes',
                         'Most_frequent_intensity',
                         'Most_frequent_intensity_last_X_notes',

                         'Amount white followed by white',
                         'Amount white followed by white last x',
                         'Amount white followed by black',
                         'Amount white followed by black last x',
                         'Amount black followed by white',
                         'Amount black followed by white last x',
                         'Amount black followed by black',
                         'Amount black followed by black last x',

                         'Most frequent three successive notes',
                         'Most frequent three successive notes last x',
                         'Most frequent Y successive notes',
                         'Most frequent Y successive notes last x',

                         'Longest sequence appeared more than once',
                         'Longest sequence appeared more than once last x',
                         'Longest sequence appeared counter',
                         'Longest sequence appeared counter last x',
                         'Longest duration sequence appeared more then once',
                         'Longest duration sequence appeared more than once last x',
                         'Longest duration sequence appeared counter',
                         'Longest duration sequence appeared counter last x',

                         "Playing time",
                         "Idle time",

                         "Average time differences between notes",
                         "Average time differences between notes_x",

                         "Average octave for the entire piece",
                         "Average octave for the last X notes",
                         "Lowest octave for the entire piece",
                         "Lowest octave for the last X notes",
                         "Highest octave for the entire piece",
                         "Highest octave for the last X not",
                         "Most frequent octave for the entire piece",
                         "Most frequent octave for the last X notes"
                         ]

FILE_BASED_FEATURES = ["Playing time entire piece",
                       "Idle time entire piece",
                       "Start time",
                       "Length of file (seconds)"]

LIST_OF_NOTES_FEATURES = ['# of times played', '# of times played last x',
                          '% time played', '% time played last x',
                          'total duration played', 'total duration last x',
                          '% duration', '% duration last x']

DURATIONS_3_SUCCESSIVE_NOTES = 'durations_3_successive_notes'
AVG_DURATION_3_SUCCESSIVE_NOTES = 'avg_duration_3_successive_notes'
AVG_DURATION_3_SUCCESSIVE_NOTES_X = 'avg_duration_3_successive_notes_x'
MOST_FREQUENT_Y_SUCCESSIVE_NOTES = 'most_frequent_y_successive_notes'
MOST_FREQUENT_Y_SUCCESSIVE_NOTES_X = 'most_frequent_y_successive_notes_x'

NOTES_SEQUENCES_COUNTER_KEY = 'notes_sequences_counter'
NOTES_SEQUENCES_COUNTER_LAST_X_KEY = 'notes_sequences_counter_x'
LONGEST_SEQUENCE_KEY = 'longest_sequence_details'
LONGEST_SEQUENCE_LAST_X_KEY = 'longest_sequence_details_x'
LONGEST_SEQ_APPEARS_COUNTER_KEY = 'LONGEST_SEQ_APPEARS_COUNTER'.lower()
LONGEST_SEQ_APPEARS_COUNTER_LAST_X_KEY = 'LONGEST_SEQ_APPEARS_COUNTER_LAST_X_KEY'.lower()
NOTES_DURATIONS_SEQUENCES_COUNTER_KEY = 'notes_durations_sequences_counter'
NOTES_DURATIONS_SEQUENCES_COUNTER_LAST_X_KEY = 'notes_durations_sequences_counter_x'
LONGEST_DURATION_SEQUENCE_KEY = 'longest_durations_sequence_details'
LONGEST_DURATION_SEQUENCE_LAST_X_KEY = 'longest_durations_sequence_details_x'
LONGEST_DURATION_SEQ_APPEARS_COUNTER_KEY = 'LONGEST_DURATION_SEQ_APPEARS_COUNTER'.lower()
LONGEST_DURATION_SEQ_APPEARS_COUNTER_LAST_X_KEY = 'LONGEST_DURATION_SEQ_APPEARS_COUNTER_LAST_X_KEY'.lower()

PLAYING_TIME_LAST_X_KEY = 'PLAYING_TIME_LAST_X'.lower()
IDLE_TIME_LAST_X_KEY = 'IDLE_TIME_LAST_X'.lower()

AVG_TIME_DIFFERENCES_KEY = 'AVG_TIME_DIFFERENCES'.lower()
AVG_TIME_DIFFERENCES_X_KEY = 'AVG_TIME_DIFFERENCES_X'.lower()

# notes features
NOTES_APPEARANCE_AND_DURATIONS_COUNTERS_DICT_KEY = "NOTES_APPEARANCE_AND_DURATIONS_COUNTERS_DICT".lower()
NOTES_APPEARANCE_AND_DURATIONS_COUNTERS_LAST_X_DICT_KEY = "NOTES_APPEARANCE_AND_DURATIONS_COUNTERS_LAST_X_DICT".lower()
TOTAL_DURATION_KEY = "TOTAL_DURATION".lower()
TOTAL_DURATION_LAST_X_KEY = "TOTAL_DURATION_LAST_X".lower()


# TOTAL_DURATIONS_COUNTERS_DICT_KEY = "TOTAL_DURATIONS_COUNTERS_DICT_KEY".lower()
# TOTAL_DURATIONS_COUNTERS_LAST_X_DICT_KEY = "TOTAL_DURATIONS_COUNTERS_LAST_X_DICT_KEY".lower()


class NoteAppearanceAndTimes:
    def __init__(self):
        self.__appearance = 0
        self.__duration = 0
        self.__appearance_percent = 0.0
        self.__duration_percent = 0.0

    @property
    def appearance(self):
        return self.__appearance

    def inc_appearance(self, v=1):
        self.__appearance += v

    def dec_appearance(self, v=1):
        self.__appearance -= v

    @property
    def duration(self):
        return self.__duration

    def inc_duration(self, v):
        self.__duration += v

    def dec_duration(self, v):
        self.__duration -= v

    def calc_percentages(self, total_appearances, total_duration):
        self.__appearance_percent = (self.appearance / total_appearances) * 100
        self.__duration_percent = (self.duration / total_duration) * 100

    @property
    def appearance_percent(self):
        return self.__appearance_percent

    @property
    def duration_percent(self):
        return self.__duration_percent


class ActiveChordDetails:
    def __init__(self, note_on_message, playing_time):
        self.note_on_message = note_on_message
        self.playing_time = playing_time

    def update_playing_time(self, val):
        self.playing_time += val


class MidiFeaturesExtractor(FeatureExtractor):
    __WHITE_NOTE = 'white'
    __BLACK_NOTE = 'black'

    __WHITE_NOTES_LIST = [21, 23, 24, 26, 28, 29, 31, 33, 35, 36, 38, 40, 41, 43, 45, 47, 48, 50, 52, 53, 55,
                          57, 59, 60, 62, 64, 65, 67, 69, 71, 72, 74, 76, 77, 79, 81, 83, 84, 86, 88, 89, 91,
                          93, 95, 96, 98, 100, 101, 103, 105, 107, 108]
    __BLACK_NOTES_LIST = [22, 25, 27, 30, 32, 34, 37, 39, 42, 44, 46, 49, 51, 54, 56, 58, 61, 63, 66, 68, 70, 73,
                          75, 78, 80, 82, 85, 87, 90, 92, 94, 97, 99, 102, 104, 106]

    __ALL_NOTES_NAMES_LIST = ["A0", "A0#", "B0",
                              "C1", "C1#", "D1", "D1#", "E1", "F1", "F1#", "G1", "G1#", "A1", "A1#", "B1",
                              "C2", "C2#", "D2", "D2#", "E2", "F2", "F2#", "G2", "G2#", "A2", "A2#", "B2",
                              "C3", "C3#", "D3", "D3#", "E3", "F3", "F3#", "G3", "G3#", "A3", "A3#", "B3",
                              "C4", "C4#", "D4", "D4#", "E4", "F4", "F4#", "G4", "G4#", "A4", "A4#", "B4",
                              "C5", "C5#", "D5", "D5#", "E5", "F5", "F5#", "G5", "G5#", "A5", "A5#", "B5",
                              "C6", "C6#", "D6", "D6#", "E6", "F6", "F6#", "G6", "G6#", "A6", "A6#", "B6",
                              "C7", "C7#", "D7", "D7#", "E7", "F7", "F7#", "G7", "G7#", "A7", "A7#", "B7",
                              "C8"]

    __OCTAVES_NOTES_NUMBERS = {
        0: [x for x in range(21, 24)],
        1: [x for x in range(24, 36)],
        2: [x for x in range(36, 48)],
        3: [x for x in range(48, 60)],
        4: [x for x in range(60, 72)],
        5: [x for x in range(72, 84)],
        6: [x for x in range(84, 96)],
        7: [x for x in range(96, 108)],
        8: [x for x in range(108, 109)]
    }

    def __init__(self, f, x, y, c, t, display_all_columns=True):
        super().__init__(f)
        self.__midi_file = mido.MidiFile(self.f)
        self.__x = x
        self.__y = y
        self.__c = c
        self.__t = t
        self.__time_differences_list = []
        self.__tempo = 500000  # 120 BPM is default if no tempo message was provided
        self.__required_channel_samples = self.get_required_channel()
        self.__display_all_columns = display_all_columns
        self.active_chords = {}
        self.active_chords_notes_to_on_index = {}
        self.__initial_intensities_avg = None
        self.__initial_time_differences_avg = None
        self.total_playing_time = 0
        self.total_idle_time = 0
        self.samples = None
        self.cumulative_playing_time = 0
        self.cumulative_idle_time = 0
        self.start_time = 0
        self.cumulative_active_chords = {}
        self.construct_time_differences_between_notes_list(self.__required_channel_samples)
        self.__result_as_list = True
        self.__logger = None
        self.set_logger()
        self.__is_first_note_arrived = False

    def set_logger(self, logger=None):
        if not logger:
            logger = logging.getLogger("midi_fe")
            logger.setLevel(logging.DEBUG)
            self.__logger = logger
        else:
            self.__logger = logger

    @classmethod
    def get_note_color(cls, sample):
        if sample.note_on_message.note in cls.__WHITE_NOTES_LIST:
            return cls.__WHITE_NOTE
        elif sample.note_on_message.note in cls.__BLACK_NOTES_LIST:
            return cls.__BLACK_NOTE
        else:
            raise Exception("invalid note found: {0}".format(sample.note_on_message.note))

    def get_lists_of_features(self):
        if self.__display_all_columns:
            # add 8 columns for each note (8 = number of subsections)
            for note_num in range(21, 109, 1):
                for note_feature in LIST_OF_NOTES_FEATURES:
                    column_name = "Note {0} ({1}) {2}".format(note_num,
                                                              self.__class__.__ALL_NOTES_NAMES_LIST[note_num - 21],
                                                              note_feature)
                    SAMPLE_BASED_FEATURES.append(column_name)

        return FILE_BASED_FEATURES, SAMPLE_BASED_FEATURES

    def calculate_avg_first_time(self):
        x_sum = 0
        for i in range(self.__x):
            x_sum += self.samples[i].note_on_message.velocity
        return x_sum / self.__x

    def calculate_avg_octave_first_time(self):
        x_sum = 0
        for i in range(self.__x):
            x_sum += self.get_octave_of_note(self.samples[i].note_on_message.note)
        return x_sum / self.__x

    def initialize_context(self):
        return {

            "intensity_features": IntensityFeatures(),

            "transitions_features": TransitionsFeatures(),
     
            '3_successive_notes_count_and_duration': {},
            'most_frequent_3_successive_notes': [],  # None,
            '3_successive_notes_count_and_duration_x': {},
            'most_frequent_3_successive_notes_x': -1,
            'y_successive_notes_count_and_duration': {},
            'most_frequent_y_successive_notes': [],  # None,
            'y_successive_notes_count_and_duration_x': {},
            'most_frequent_y_successive_notes_x': -1,

            NOTES_SEQUENCES_COUNTER_KEY: {},
            LONGEST_SEQUENCE_KEY: (),
            NOTES_SEQUENCES_COUNTER_LAST_X_KEY: {},
            LONGEST_SEQUENCE_LAST_X_KEY: (),
            LONGEST_SEQ_APPEARS_COUNTER_KEY: 0,
            LONGEST_SEQ_APPEARS_COUNTER_LAST_X_KEY: 0,

            NOTES_DURATIONS_SEQUENCES_COUNTER_KEY: {},
            LONGEST_DURATION_SEQUENCE_KEY: (),
            NOTES_DURATIONS_SEQUENCES_COUNTER_LAST_X_KEY: {},
            LONGEST_DURATION_SEQUENCE_LAST_X_KEY: (),
            LONGEST_DURATION_SEQ_APPEARS_COUNTER_KEY: 0,
            LONGEST_DURATION_SEQ_APPEARS_COUNTER_LAST_X_KEY: 0,

            PLAYING_TIME_LAST_X_KEY: 0,
            IDLE_TIME_LAST_X_KEY: 0,

            AVG_TIME_DIFFERENCES_KEY: 0,

            "octave_features": OctaveFeatures(),

            # notes
            NOTES_APPEARANCE_AND_DURATIONS_COUNTERS_DICT_KEY: {key: NoteAppearanceAndTimes() for key in
                                                               range(21, 109, 1)},
            NOTES_APPEARANCE_AND_DURATIONS_COUNTERS_LAST_X_DICT_KEY: {key: NoteAppearanceAndTimes() for key in
                                                                      range(21, 109, 1)},
            TOTAL_DURATION_KEY: 0,
            TOTAL_DURATION_LAST_X_KEY: 0
        }

    def update_time_features(self, current_features, sample, sample_index, raw_index, is_last_sample=False):
        self.calc_cumulative_playing_and_idle_times(self.__required_channel_samples[raw_index])
        self.calc_cumulative_playing_and_idle_times(self.__required_channel_samples[raw_index + 1])
        current_features[PLAYING_TIME_LAST_X_KEY] = self.cumulative_playing_time
        current_features[IDLE_TIME_LAST_X_KEY] = self.cumulative_idle_time

        if is_last_sample:
            self.total_playing_time, self.total_idle_time = self.cumulative_playing_time, self.cumulative_idle_time

    @staticmethod
    def update_appearance_and_duration_percentage(notes, total_appearance, total_duration):
        for note in notes:
            notes[note].calc_percentages(total_appearance, total_duration)

    def update_notes_features(self, current_features, sample, sample_index):
        # update total
        notes = current_features[NOTES_APPEARANCE_AND_DURATIONS_COUNTERS_DICT_KEY]
        notes[sample.note_on_message.note].inc_appearance()
        notes[sample.note_on_message.note].inc_duration(sample.playing_time)

        current_features[TOTAL_DURATION_KEY] += sample.playing_time
        self.update_appearance_and_duration_percentage(notes, sample_index + 1, current_features[TOTAL_DURATION_KEY])

        # notes[sample.note_on_message.note].calc_percentages(sample_index + 1, current_features[TOTAL_DURATION_KEY])

        # update last x dict
        notes_x = current_features[NOTES_APPEARANCE_AND_DURATIONS_COUNTERS_LAST_X_DICT_KEY]
        notes_x[sample.note_on_message.note].inc_appearance()
        notes_x[sample.note_on_message.note].inc_duration(sample.playing_time)
        current_features[TOTAL_DURATION_LAST_X_KEY] += sample.playing_time

        current_features.update(notes)
        current_features.update(notes_x)

        if sample_index > self.__x:
            notes_x = current_features[NOTES_APPEARANCE_AND_DURATIONS_COUNTERS_LAST_X_DICT_KEY]

            # getting oldest sample
            oldest_sample_index = (sample_index + 1) - self.__x
            oldest_sample = self.samples[oldest_sample_index]

            # updating total duration
            current_features[TOTAL_DURATION_LAST_X_KEY] -= oldest_sample.playing_time
            current_features[TOTAL_DURATION_LAST_X_KEY] += sample.playing_time

            # updating oldest sample details
            notes_x[oldest_sample.note_on_message.note].dec_appearance()
            notes_x[oldest_sample.note_on_message.note].dec_duration(oldest_sample.playing_time)

            # update new sample details
            notes_x[sample.note_on_message.note].inc_appearance()
            notes_x[sample.note_on_message.note].inc_duration(sample.playing_time)

            self.update_appearance_and_duration_percentage(notes_x, self.__x,
                                                           current_features[TOTAL_DURATION_LAST_X_KEY])

            current_features.update(notes_x)

    def update_intensity_features(self, current_features, sample, sample_index):
        self.__logger.debug("extracting intensity features")
        intensity = current_features["intensity_features"]  # type: IntensityFeatures
        intensity.min = min(intensity.min, sample.note_on_message.velocity)
        intensity.max = max(intensity.max, sample.note_on_message.velocity)
        intensity.avg = intensity.avg * (sample_index / (sample_index + 1)) + (
                sample.note_on_message.velocity / (sample_index + 1))

        if sample.note_on_message.velocity in intensity.count:
            intensity.count[sample.note_on_message.velocity] += 1
        else:
            intensity.count[sample.note_on_message.velocity] = 1

        if not intensity.most_frequent:
            intensity.most_frequent = sample.note_on_message.velocity

        if intensity.count[intensity.most_frequent] < intensity.count[sample.note_on_message.velocity]:
            intensity.most_frequent = sample.note_on_message.velocity

        if sample.note_on_message.velocity in intensity.count_x:
            intensity.count_x[sample.note_on_message.velocity] += 1
        else:
            intensity.count_x[sample.note_on_message.velocity] = 1

        if sample_index + 1 >= self.__x:
            intensity.min_x = min(self.samples[sample_index - self.__x + 1:sample_index + 1],
                                  key=lambda a: a.note_on_message.velocity).note_on_message.velocity
            intensity.max_x = max(self.samples[sample_index - self.__x + 1:sample_index + 1],
                                  key=lambda a: a.note_on_message.velocity).note_on_message.velocity

            if intensity.avg_x == -1:
                intensity.avg_x = ((self.calculate_avg_first_time() * self.__x) - self.samples[
                    sample_index + 1 - self.__x]. \
                                   note_on_message.velocity + sample.note_on_message.velocity) / self.__x
            else:
                intensity.avg_x = ((intensity.avg_x * self.__x) - self.samples[
                    sample_index + 1 - self.__x].note_on_message.velocity + sample.note_on_message.velocity) / self.__x

            if intensity.most_frequent_x == -1:
                intensity.most_frequent_x = sample.note_on_message.velocity

            if sample_index != 2:
                oldest_sample_to_remove_index = (sample_index + 1) - self.__x
                oldest_sample_to_remove = self.samples[oldest_sample_to_remove_index]
                intensity.count_x[oldest_sample_to_remove.note_on_message.velocity] -= 1

            if intensity.count_x[intensity.most_frequent_x] < intensity.count_x[sample.note_on_message.velocity]:
                intensity.most_frequent_x = sample.note_on_message.velocity

    def get_octave_of_note(self, note):
        for octave in self.__OCTAVES_NOTES_NUMBERS.keys():
            if note in self.__OCTAVES_NOTES_NUMBERS[octave]:
                return octave

    def update_octaves_features(self, current_features, sample, sample_index):
        self.__logger.debug("extracting octaves features")
        octaves = current_features["octave_features"]  # type: OctaveFeatures
        current_octave = self.get_octave_of_note(sample.note_on_message.note)
        octaves.min = min(octaves.min, current_octave)
        octaves.max = max(octaves.max, current_octave)

        octaves.avg = octaves.avg * (sample_index / (sample_index + 1)) + (current_octave / (sample_index + 1))

        if current_octave in octaves.count:
            octaves.count[current_octave] += 1
        else:
            octaves.count[current_octave] = 1

        if not octaves.most_frequent:
            octaves.most_frequent = current_octave

        if octaves.count[octaves.most_frequent] < octaves.count[current_octave]:
            octaves.most_frequent = current_octave

        if current_octave in octaves.count_x:
            octaves.count_x[current_octave] += 1
        else:
            octaves.count_x[current_octave] = 1

        if sample_index + 1 >= self.__x:
            octaves.min_x = self.get_octave_of_note(
                min(self.samples[sample_index - self.__x + 1:sample_index + 1],
                    key=lambda a: self.get_octave_of_note(a.note_on_message.note)).note_on_message.note)

            octaves.max_x = self.get_octave_of_note(
                max(self.samples[sample_index - self.__x + 1:sample_index + 1],
                    key=lambda a: self.get_octave_of_note(a.note_on_message.note)).note_on_message.note)

            if octaves.avg_x == -1:
                octaves.avg_x = self.calculate_avg_octave_first_time()

            octaves.avg_x = ((octaves.avg_x * self.__x) - self.get_octave_of_note(
                self.samples[sample_index + 1 - self.__x].note_on_message.note) +
                             self.get_octave_of_note(sample.note_on_message.note)) / self.__x

            if octaves.most_frequent_x == -1:
                octaves.most_frequent_x = self.get_octave_of_note(sample.note_on_message.note)

            if sample_index != 2:
                oldest_sample_to_remove_index = (sample_index + 1) - self.__x
                oldest_sample_to_remove = self.samples[oldest_sample_to_remove_index]
                octaves.count_x[self.get_octave_of_note(oldest_sample_to_remove.note_on_message.note)] -= 1

            if octaves.count_x[octaves.most_frequent_x] < octaves.count_x[self.get_octave_of_note(sample.note_on_message.note)]:
                octaves.most_frequent_x = self.get_octave_of_note(sample.note_on_message.note)

    def get_oldest_y_notes(self, oldest_index, y):
        current = 1
        oldest_y_samples = (self.samples[oldest_index].note_on_message.note,)
        while current != y:
            oldest_y_samples = oldest_y_samples + (self.samples[oldest_index + current].note_on_message.note,)
            current += 1
        return oldest_y_samples

    def get_oldest_y_notes_duration(self, oldest_index, y):
        current = 1
        oldest_y_samples = (self.samples[oldest_index].playing_time,)
        while current != y:
            oldest_y_samples = oldest_y_samples + (self.samples[oldest_index + current].playing_time,)
            current += 1
        return oldest_y_samples

    def get_last_y_notes(self, sample_index, y):
        real_y = y - 1
        last_y_samples = ()
        while real_y != 0:
            last_y_samples = last_y_samples + (self.samples[sample_index - real_y].note_on_message.note,)
            real_y -= 1
        last_y_samples = last_y_samples + (self.samples[sample_index].note_on_message.note,)
        return last_y_samples

    def get_last_y_notes_durations(self, sample_index, y):
        real_y = y - 1
        last_y_durations = ()
        while real_y != 0:
            last_y_durations = last_y_durations + (self.samples[sample_index - real_y].playing_time,)
            real_y -= 1
        last_y_durations = last_y_durations + (self.samples[sample_index].playing_time,)
        return last_y_durations

    def update_clusters_features_for_y_successive_notes(self, current_features, sample_index, y,
                                                        notes_count_key_name,
                                                        notes_count_key_name_x,
                                                        most_frquent_key_name,
                                                        most_freuqent_x_key_name):
        if sample_index < (y - 1):
            return
        current_y = self.get_last_y_notes(sample_index, y)

        if not current_features[notes_count_key_name].get(current_y, None):
            current_features[notes_count_key_name][current_y] = {'count': 1, 'duration': ''}
            current_features[notes_count_key_name_x][current_y] = {'count': 1, 'duration': ''}
        else:
            current_features[notes_count_key_name][current_y]['count'] += 1
            current_features[notes_count_key_name_x][current_y]['count'] += 1

        if not current_features[most_frquent_key_name]:
            current_features[most_frquent_key_name] = current_y

        else:
            if current_features[notes_count_key_name][current_y]['count'] > \
                    current_features[notes_count_key_name][current_features[most_frquent_key_name]]['count']:
                current_features[most_frquent_key_name] = current_y

        if sample_index >= self.__x + (y - 1):
            # re-construct oldest three notes and decrease it counter by 1
            oldest_index = (sample_index + 1) - self.__x

            oldest_y = self.get_oldest_y_notes(oldest_index, y)

            current_features[notes_count_key_name_x][oldest_y]['count'] -= 1

            if current_features[most_freuqent_x_key_name] == -1:
                current_features[most_freuqent_x_key_name] = current_features[most_frquent_key_name]

            else:
                if current_features[most_freuqent_x_key_name] == oldest_y:
                    # this is the only case where the most frequent 3 may change
                    max_3_details_dict = max(list(current_features[notes_count_key_name_x].items()),
                                             key=lambda x: x[1]['count'])
                    max_3 = max_3_details_dict[0]
                    current_features[most_freuqent_x_key_name] = max_3

    def update_clusters_features(self, current_features, sample, sample_index):
        self.__logger.debug("extracting clusters features")
        current_notes_count_key_name = '{0}_successive_notes_count_and_duration'.format(3)
        current_notes_count_x_key_name = '{0}_successive_notes_count_and_duration_x'.format(3)
        most_frequent_successive_notes_key_name = 'most_frequent_{0}_successive_notes'.format(3)
        most_frequent_successive_notes_x_key_name = 'most_frequent_{0}_successive_notes_x'.format(3)
        self.update_clusters_features_for_y_successive_notes(
            current_features, sample_index, 3, current_notes_count_key_name, current_notes_count_x_key_name,
            most_frequent_successive_notes_key_name, most_frequent_successive_notes_x_key_name)

        current_notes_count_key_name = 'y_successive_notes_count_and_duration'
        current_notes_count_x_key_name = 'y_successive_notes_count_and_duration_x'
        most_frequent_successive_notes_key_name = 'most_frequent_y_successive_notes'
        most_frequent_successive_notes_x_key_name = 'most_frequent_y_successive_notes_x'
        self.update_clusters_features_for_y_successive_notes(
            current_features, sample_index, self.__y, current_notes_count_key_name, current_notes_count_x_key_name,
            most_frequent_successive_notes_key_name, most_frequent_successive_notes_x_key_name)

        # average duration
        # TODO: WHAT IS AVERAGE DURATION BETWEEN NOTES? HOW IS IT DEFINED?

    def update_transitions_features(self, current_features, sample, sample_index):
        self.__logger.debug("extracting transitions features")
        transitions = current_features["transitions_features"]  # type: TransitionsFeatures
        if sample_index == 0:
            c_s_color = self.__class__.get_note_color(sample)
            transitions.last_x_notes_colors[sample_index] = c_s_color
            return

        p_s_color = self.__class__.get_note_color(self.samples[sample_index - 1])
        c_s_color = self.__class__.get_note_color(sample)
        if p_s_color == self.__class__.__WHITE_NOTE and c_s_color == self.__class__.__WHITE_NOTE:
            transitions.ww += 1
            transitions.ww_x += 1
        elif p_s_color == self.__class__.__WHITE_NOTE and c_s_color == self.__class__.__BLACK_NOTE:
            transitions.wb += 1
            transitions.wb_x += 1
        elif p_s_color == self.__class__.__BLACK_NOTE and c_s_color == self.__class__.__WHITE_NOTE:
            transitions.bw += 1
            transitions.bw_x += 1
        elif p_s_color == self.__class__.__BLACK_NOTE and c_s_color == self.__class__.__BLACK_NOTE:
            transitions.bb += 1
            transitions.bb_x += 1

        transitions.last_x_notes_colors[sample_index] = c_s_color

        if sample_index >= self.__x:
            oldest_sample_to_remove_index = (sample_index + 1) - self.__x
            oldest_sample_to_remove = self.samples[oldest_sample_to_remove_index]
            o_s_color = self.__class__.get_note_color(oldest_sample_to_remove)

            second_oldest_sample_to_remove_index = ((sample_index + 1) - self.__x) + 1
            second_oldest_sample_to_remove = self.samples[second_oldest_sample_to_remove_index]
            so_s_color = self.__class__.get_note_color(second_oldest_sample_to_remove)

            if o_s_color == self.__class__.__WHITE_NOTE and so_s_color == self.__class__.__WHITE_NOTE:
                transitions.ww_x -= 1
            elif o_s_color == self.__class__.__WHITE_NOTE and so_s_color == self.__class__.__BLACK_NOTE:
                transitions.wb_x -= 1
            elif o_s_color == self.__class__.__BLACK_NOTE and so_s_color == self.__class__.__WHITE_NOTE:
                transitions.bw_x -= 1
            elif o_s_color == self.__class__.__BLACK_NOTE and so_s_color == self.__class__.__BLACK_NOTE:
                transitions.bb_x -= 1

    def update_repetitions_features(self, current_features, sample, sample_index):
        self.__logger.debug("extracting repetitions features")
        num_of_sequences = min(sample_index + 1, 10)
        # notes sequences
        for seq_length in range(num_of_sequences, 0, -1):
            current_seq = self.get_last_y_notes(sample_index, seq_length)
            if not current_features[NOTES_SEQUENCES_COUNTER_KEY].get(current_seq, None):
                current_features[NOTES_SEQUENCES_COUNTER_KEY][current_seq] = 1
                current_features[NOTES_SEQUENCES_COUNTER_LAST_X_KEY][current_seq] = 1
            else:
                current_features[NOTES_SEQUENCES_COUNTER_KEY][current_seq] += 1
                current_features[NOTES_SEQUENCES_COUNTER_LAST_X_KEY][current_seq] += 1
                if len(current_seq) > len(current_features[LONGEST_SEQUENCE_KEY]):
                    current_features[LONGEST_SEQUENCE_KEY] = current_seq
                    break

        current_features[LONGEST_SEQ_APPEARS_COUNTER_KEY] = current_features[NOTES_SEQUENCES_COUNTER_KEY].get(
            current_features[LONGEST_SEQUENCE_KEY], 0)

        # notes duration sequences
        for seq_length in range(num_of_sequences, 0, -1):
            current_duration_seq = self.get_last_y_notes_durations(sample_index, seq_length)
            if not current_features[NOTES_DURATIONS_SEQUENCES_COUNTER_KEY].get(current_duration_seq, None):
                current_features[NOTES_DURATIONS_SEQUENCES_COUNTER_KEY][current_duration_seq] = 1
                current_features[NOTES_DURATIONS_SEQUENCES_COUNTER_LAST_X_KEY][current_duration_seq] = 1
            else:
                current_features[NOTES_DURATIONS_SEQUENCES_COUNTER_KEY][current_duration_seq] += 1
                current_features[NOTES_DURATIONS_SEQUENCES_COUNTER_LAST_X_KEY][current_duration_seq] += 1
                if len(current_duration_seq) > len(current_features[LONGEST_DURATION_SEQUENCE_KEY]):
                    current_features[LONGEST_DURATION_SEQUENCE_KEY] = current_duration_seq
                    break

        current_features[LONGEST_DURATION_SEQ_APPEARS_COUNTER_KEY] = current_features[
            NOTES_DURATIONS_SEQUENCES_COUNTER_KEY].get(
            current_features[LONGEST_DURATION_SEQUENCE_KEY], 0)

        if sample_index + 1 > self.__x:
            num_of_sequences = min(self.__x, 10)

            for seq_length in range(num_of_sequences, 0, -1):
                oldest_index = sample_index - self.__x
                current_seq = self.get_oldest_y_notes(oldest_index, seq_length)
                current_duration_seq = self.get_oldest_y_notes_duration(oldest_index, seq_length)
                current_features[NOTES_SEQUENCES_COUNTER_LAST_X_KEY][current_seq] -= 1
                current_features[NOTES_DURATIONS_SEQUENCES_COUNTER_LAST_X_KEY][current_duration_seq] -= 1

            for seq_length in range(num_of_sequences, 0, -1):
                oldest_index = (sample_index + 1) - self.__x
                current_seq = self.get_oldest_y_notes(oldest_index, seq_length)
                current_duration_seq = self.get_oldest_y_notes_duration(oldest_index, seq_length)

                if current_features[NOTES_SEQUENCES_COUNTER_LAST_X_KEY][current_seq] > 1 and \
                        len(current_seq) > len(current_features[LONGEST_SEQUENCE_LAST_X_KEY]):
                    current_features[LONGEST_SEQUENCE_LAST_X_KEY] = current_seq

                if current_features[NOTES_DURATIONS_SEQUENCES_COUNTER_LAST_X_KEY][current_duration_seq] > 1 and \
                        len(current_duration_seq) > len(current_features[LONGEST_DURATION_SEQUENCE_LAST_X_KEY]):
                    current_features[LONGEST_DURATION_SEQUENCE_LAST_X_KEY] = current_duration_seq

            if current_features[LONGEST_SEQUENCE_LAST_X_KEY] != ():
                if current_features[NOTES_SEQUENCES_COUNTER_LAST_X_KEY][
                    current_features[LONGEST_SEQUENCE_LAST_X_KEY]] <= 1:
                    current_features[LONGEST_SEQUENCE_LAST_X_KEY] = ()

            if current_features[LONGEST_DURATION_SEQUENCE_LAST_X_KEY] != ():
                if current_features[NOTES_DURATIONS_SEQUENCES_COUNTER_LAST_X_KEY][
                    current_features[LONGEST_DURATION_SEQUENCE_LAST_X_KEY]] <= 1:
                    current_features[LONGEST_DURATION_SEQUENCE_LAST_X_KEY] = ()

            current_features[LONGEST_SEQ_APPEARS_COUNTER_LAST_X_KEY] = current_features[
                NOTES_SEQUENCES_COUNTER_LAST_X_KEY].get(
                current_features[LONGEST_SEQUENCE_LAST_X_KEY], 0)

            current_features[LONGEST_DURATION_SEQ_APPEARS_COUNTER_LAST_X_KEY] = current_features[
                NOTES_DURATIONS_SEQUENCES_COUNTER_LAST_X_KEY].get(
                current_features[LONGEST_DURATION_SEQUENCE_LAST_X_KEY], 0)

    def calc_initial_time_differences_avg(self):
        x_sum = 0
        for i in range(self.__x):
            x_sum += self.__time_differences_list[i]
        return x_sum / self.__x

    def time_difference_over_t(self, sample_index):
        time_difference = self.__time_differences_list[sample_index]
        if time_difference > self.__t:
            self.__logger.info(
                "ignoring time difference ({0}) as it is bigger than t={1}".format(time_difference, self.__t))
            return True
        return False

    def update_rhythm_features(self, current_features, sample, sample_index):
        self.__logger.debug("extracting rhythm features")
        if self.time_difference_over_t(sample_index):
            return
        current_features[AVG_TIME_DIFFERENCES_KEY] = current_features[AVG_TIME_DIFFERENCES_KEY] * (
                sample_index / (sample_index + 1)) + (self.__time_differences_list[sample_index] / (sample_index + 1))

        if sample_index + 1 >= self.__x:
            current_avg = current_features.get(AVG_TIME_DIFFERENCES_X_KEY, self.calc_initial_time_differences_avg())
            current_features[AVG_TIME_DIFFERENCES_X_KEY] = ((current_avg * self.__x) -
                                                            self.__time_differences_list[sample_index + 1 - self.__x] +
                                                            self.__time_differences_list[sample_index]) / self.__x

    def update_sample_based_features(self, current_features, sample, sample_index):
        if sample_index == len(self.samples) - 1:
            self.update_time_features(current_features, sample, sample_index, sample_index * 2, is_last_sample=True)
        else:
            self.update_time_features(current_features, sample, sample_index, sample_index * 2)

        self.update_notes_features(current_features, sample, sample_index)
        self.update_intensity_features(current_features, sample, sample_index)
        self.update_octaves_features(current_features, sample, sample_index)
        self.update_clusters_features(current_features, sample, sample_index)
        self.update_transitions_features(current_features, sample, sample_index)
        self.update_repetitions_features(current_features, sample, sample_index)
        self.update_rhythm_features(current_features, sample, sample_index)

    def extract_file_based_features(self):
        return [self.cumulative_playing_time, self.cumulative_idle_time, self.start_time, self.__midi_file.length]

    @staticmethod
    def get_notes_features_as_list(current_features):
        all_notes = []
        for i, note in enumerate(range(21, 109, 1)):
            note_total = current_features[NOTES_APPEARANCE_AND_DURATIONS_COUNTERS_DICT_KEY][
                note]  # type: NoteAppearanceAndTimes
            note_total_last_x = current_features[NOTES_APPEARANCE_AND_DURATIONS_COUNTERS_LAST_X_DICT_KEY][
                note]  # type: NoteAppearanceAndTimes
            all_notes.extend([note_total.appearance, note_total_last_x.appearance,
                              note_total.appearance_percent, note_total_last_x.appearance_percent,
                              note_total.duration, note_total_last_x.duration,
                              note_total.duration_percent, note_total_last_x.duration_percent])
        return all_notes

    def extract_sample_based_features(self, sample, sample_index, current_features):
        self.__logger.info("extracting sample based features")
        try:
            self.update_sample_based_features(current_features, sample, sample_index)
            intensity = current_features["intensity_features"]  # type: IntensityFeatures
            octave = current_features["octave_features"]  # type: OctaveFeatures
            transitions = current_features["transitions_features"]  # type: TransitionsFeatures

            features_list = [sample.note_on_message.note, sample.note_on_message.velocity, sample.playing_time,
                             intensity.min,
                             intensity.min_x,
                             intensity.max,
                             intensity.max_x,
                             intensity.avg,
                             intensity.avg_x,
                             intensity.most_frequent,
                             intensity.most_frequent_x,

                             transitions.ww,
                             transitions.ww_x,
                             transitions.bw,
                             transitions.bw_x,
                             transitions.wb,
                             transitions.wb_x,
                             transitions.bb,
                             transitions.bb_x,

                             current_features['most_frequent_3_successive_notes'],
                             current_features['most_frequent_3_successive_notes_x'],
                             current_features['most_frequent_y_successive_notes'],
                             current_features['most_frequent_y_successive_notes_x'],

                             # Repetitions
                             current_features[LONGEST_SEQUENCE_KEY],
                             current_features[LONGEST_SEQUENCE_LAST_X_KEY],
                             current_features[LONGEST_SEQ_APPEARS_COUNTER_KEY],
                             current_features[LONGEST_SEQ_APPEARS_COUNTER_LAST_X_KEY],
                             current_features[LONGEST_DURATION_SEQUENCE_KEY],
                             current_features[LONGEST_DURATION_SEQUENCE_LAST_X_KEY],
                             current_features[LONGEST_DURATION_SEQ_APPEARS_COUNTER_KEY],
                             current_features[LONGEST_DURATION_SEQ_APPEARS_COUNTER_LAST_X_KEY],

                             current_features[PLAYING_TIME_LAST_X_KEY],
                             current_features[IDLE_TIME_LAST_X_KEY],

                             current_features[AVG_TIME_DIFFERENCES_KEY],
                             current_features.get(AVG_TIME_DIFFERENCES_X_KEY, -1),


                             octave.avg,
                             octave.avg_x,
                             octave.min,
                             octave.min_x,
                             octave.max,
                             octave.max_x,
                             octave.most_frequent,
                             octave.most_frequent_x]

            features_list.extend(self.get_notes_features_as_list(current_features))

            features_dict = {
                "sessionId": 1,
                "analysis": [{
                    "note": {
                        "note": sample.note_on_message.note,
                        "param1": sample.note_on_message.velocity,
                        "param2": sample.note_on_message.time
                    },
                    "chanel": self.__c,
                    "userId": "5c5c363eb2a6a83c9fd7245f",
                    "controllerId": "5c5c363eb2a6a83c9fd7245f",

                    "time": {
                        "playing_time_for_last_x_notes": current_features[PLAYING_TIME_LAST_X_KEY],
                        "idle_time_for_last_x_notes": current_features[IDLE_TIME_LAST_X_KEY]
                    },
                    "intensity": {
                        "intensity": sample.note_on_message.velocity,
                        "lowest_intensity_entire_piece": intensity.min,
                        "lowest_intensity_last_x_notes": intensity.min_x,
                        "highest_intensity_entire_piece": intensity.max,
                        "highest_intensity_last_x_notes": intensity.max_x,
                        "average_intensity_entire_piece": intensity.avg,
                        "average_intensity_last_X_notes": intensity.avg_x,
                        "most_frequent_intensity_entire_piece": intensity.most_frequent,
                        "most_frequent_intensity_last_X_notes": intensity.most_frequent_x,
                    },
                    "octaves": {
                        "average_octave_entire_piece": octave.avg,
                        "average_octave_last_X_notes": octave.avg_x,
                        "lowest_octave_entire_piece": octave.min,
                        "lowest_octave_last_X_notes": octave.min_x,
                        "highest_octave_entire_piece": octave.max,
                        "highest_octave_last_X_notes": octave.max_x,
                        "most_frequent_octave_entire_piece": octave.most_frequent,
                        "most_frequent_octave_last_X_notes": octave.most_frequent_x
                    },
                    "transitions": {
                        "amount_white_followed_by_white": transitions.ww,
                        "amount_white_followed_by_white_last_x": transitions.ww_x,
                        "amount_white_followed_by_black": transitions.bw,
                        "amount_white_followed_by_black_last_x": transitions.bw_x,
                        "amount_black_followed_by_white": transitions.wb,
                        "amount_black_followed_by_white_last_x": transitions.wb_x,
                        "amount_black_followed_by_black": transitions.bb,
                        "amount_black_followed_by_black_last_x": transitions.bb_x
                    },
                    "repetitions": {
                        "longest_sequence_appeared_more_than_once": list(current_features[LONGEST_SEQUENCE_KEY]),
                        "longest_sequence_appeared_more_than_once_last_x": list(
                            current_features[LONGEST_SEQUENCE_LAST_X_KEY]),
                        "amount_of_times_that_the_longest_sequence_of_notes_that_appeared_for_entire_piece":
                            current_features[LONGEST_SEQ_APPEARS_COUNTER_KEY],
                        "amount_of_times_that_the_longest_sequence_of_notes_that_appeared_for_the_last_X_notes":
                            current_features[LONGEST_SEQ_APPEARS_COUNTER_LAST_X_KEY],

                        "longest_durations_sequence_appeared_more_than_once":
                            current_features[LONGEST_DURATION_SEQUENCE_KEY],
                        "longest_durations_sequence_appeared_more_than_once_last_x":
                            current_features[LONGEST_DURATION_SEQUENCE_LAST_X_KEY],
                        "amount_of_times_longest_sequence_of_notes_duration_differences_appeared_for_the_entire_piece":
                            current_features[LONGEST_DURATION_SEQ_APPEARS_COUNTER_KEY],
                        "amount_of_times_longest_sequence_of_notes_duration_differences_appeared_last_X_notes":
                            current_features[LONGEST_DURATION_SEQ_APPEARS_COUNTER_LAST_X_KEY],

                    },
                    "clusters": {
                        "most_frequent_three_successive_notes": current_features[
                            'most_frequent_3_successive_notes'],
                        "most_frequent_three_successive_notes_last_x": current_features[
                            'most_frequent_3_successive_notes_x'],
                        "most_frequent_Y_successive_notes": current_features['most_frequent_y_successive_notes'],
                        "most_frequent_Y_successive_notes_last_x": current_features[
                            'most_frequent_y_successive_notes_x'],
                    },
                    "rhythm": {
                        "average_time_differences_between_notes_where_differences_over_T_for_the_entire_piece":
                            current_features[AVG_TIME_DIFFERENCES_KEY],
                        "average_time_differences_between_notes_where_differences_over_T_for_last_x_notes":
                            current_features.get(AVG_TIME_DIFFERENCES_X_KEY, -1)
                    }
                }]
            }
            return features_list, features_dict

        except Exception as e:
            print(str(e))

    def calc_cumulative_playing_and_idle_times(self, sample):
        if not self.__is_first_note_arrived:
            delta = mido.tick2second(sample.time, self.__midi_file.ticks_per_beat, self.__tempo)
            self.start_time += delta
            # self.cumulative_idle_time += delta
        if sample.type == 'note_on' or sample.type == 'note_off':
            self.__is_first_note_arrived = True
            delta_in_sec = mido.tick2second(sample.time, self.__midi_file.ticks_per_beat, self.__tempo)
            if self.is_note_start(sample):
                if self.cumulative_active_chords:
                    # there are playing chords
                    self.cumulative_playing_time += delta_in_sec
                else:
                    # no chords are playing
                    self.cumulative_idle_time += delta_in_sec
                self.cumulative_active_chords[sample.note] = True

            elif self.is_note_end(sample):
                if sample.note in self.cumulative_active_chords:
                    # there are playing chords
                    self.cumulative_playing_time += delta_in_sec
                    del self.cumulative_active_chords[sample.note]
                else:
                    # midi file contains stopping a single note two times in a row so ignoring the second time.
                    pass

    def calc_notes_times(self, messages):
        ordered_notes_with_times = OrderedDict()
        for i, sample in enumerate(messages):
            i += 10000
            if sample.type == 'note_on' or sample.type == 'note_off':
                single_note_identifier = '{1}_{0}'.format(sample.note, i)
                if self.is_note_start(sample):
                    ordered_notes_with_times[single_note_identifier] = None
                    self.active_chords_notes_to_on_index[sample.note] = i
                    for key in self.active_chords:
                        current = self.active_chords[key]
                        current.update_playing_time(sample.time)

                    active_chord = ActiveChordDetails(sample, 0)
                    self.active_chords[sample.note] = active_chord

                elif self.is_note_end(sample):
                    if sample.note in self.active_chords:
                        for key in self.active_chords:
                            current = self.active_chords[key]  # type: ActiveChordDetails
                            current.update_playing_time(sample.time)

                        finished_chord = self.active_chords[sample.note]
                        final_dict_index = '{1}_{0}'.format(sample.note,
                                                            self.active_chords_notes_to_on_index[sample.note])

                        ordered_notes_with_times[final_dict_index] = finished_chord
                        del self.active_chords[sample.note]
                    else:
                        # midi file contains stopping a single note two times in a row so ignoring the second time.
                        pass

        return ordered_notes_with_times

    def construct_time_differences_between_notes_list(self, messages):
        ticks_delta_since_last_start_of_note = 0
        for sample in messages:
            if self.is_note_start(sample):
                ticks_delta = ticks_delta_since_last_start_of_note + sample.time
                seconds_delta = mido.tick2second(ticks_delta, self.__midi_file.ticks_per_beat, self.__tempo)
                self.__time_differences_list.append(seconds_delta)
                ticks_delta_since_last_start_of_note = 0
            elif self.is_note_end(sample):
                ticks_delta_since_last_start_of_note += sample.time

    def get_samples(self):
        samples_list = []
        # midi structure is good for real time playing but not for analyzing (some of the features),
        # so reconstruction is needed.
        samples_dict_with_abs_times = self.calc_notes_times(self.__required_channel_samples)
        for key in samples_dict_with_abs_times:
            if samples_dict_with_abs_times[key]:
                samples_list.append(samples_dict_with_abs_times[key])
        self.samples = samples_list
        return self.samples

    def get_required_channel(self):
        single_channel_data_samples = []
        for track in self.__midi_file.tracks:
            for msg in track:
                if not msg.is_meta and msg.channel == self.__c:
                    single_channel_data_samples.append(msg)
                else:
                    if msg.type == "set_tempo":
                        print("Found 'set tempo' message: tempo of file is {0} BPM".format(msg.tempo))
                        print("bpm is {0}".format(mido.tempo2bpm(msg.tempo)))
                        self.__tempo = msg.tempo
        return single_channel_data_samples

    @staticmethod
    def is_note_message(message):
        return True if message.type == 'note_on' or message.type == 'note_off' else False

    @staticmethod
    def is_note_start(message):
        if message.type == 'note_on' and message.velocity != 0:
            return True

    @staticmethod
    def is_note_end(message):
        if (message.type == 'note_on' and message.velocity == 0) or (message.type == 'note_off'):
            return True

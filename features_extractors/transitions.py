import sys


class TransitionsFeatures:
    def __init__(self):
        self.__ww = 0
        self.__ww_x = 0
        self.__wb = 0
        self.__wb_x = 0
        self.__bw = 0
        self.__bw_x = 0
        self.__bb = 0
        self.__bb_x = 0

        self.last_x_notes_colors = {}

    @property
    def ww(self):
        return self.__ww

    @ww.setter
    def ww(self, v):
        self.__ww = v

    @property
    def wb(self):
        return self.__wb

    @wb.setter
    def wb(self, v):
        self.__wb = v

    @property
    def bw(self):
        return self.__bw

    @bw.setter
    def bw(self, v):
        self.__bw = v

    @property
    def bb(self):
        return self.__bb

    @bb.setter
    def bb(self, v):
        self.__bb = v

    @property
    def ww_x(self):
        return self.__ww_x

    @ww_x.setter
    def ww_x(self, v):
        self.__ww_x = v

    @property
    def wb_x(self):
        return self.__wb_x

    @wb_x.setter
    def wb_x(self, v):
        self.__wb_x = v

    @property
    def bw_x(self):
        return self.__bw_x

    @bw_x.setter
    def bw_x(self, v):
        self.__bw_x = v

    @property
    def bb_x(self):
        return self.__bb_x

    @bb_x.setter
    def bb_x(self, v):
        self.__bb_x = v

    @staticmethod
    def pretty(attr):
        l = attr.split('_')
        l = [w.capitalize() for w in l]
        if l[-1] == 'X':
            l[-1] = 'Octave Last X Notes'
        else:
            l.append("Octave")
        return ' '.join(l)

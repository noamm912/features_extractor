from abc import ABC, abstractmethod


class FeatureExtractor(ABC):

    def __init__(self, f):
        self.f = f

    @abstractmethod
    def extract_sample_based_features(self, sample, sample_index, current_features):
        pass

    @abstractmethod
    def extract_file_based_features(self):
        pass

    @abstractmethod
    def get_samples(self):
        pass

    @abstractmethod
    def get_lists_of_features(self):
        pass

    @abstractmethod
    def initialize_context(self):
        pass

    @abstractmethod
    def set_logger(self, logger):
        pass

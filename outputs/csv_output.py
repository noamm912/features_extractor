from outputs.output_layer import OutputLayer
import csv


class CsvOutput(OutputLayer):

    def __init__(self, **kwargs):
        super().__init__("list")
        self.__filename = kwargs["file_name"]
        self.__file = None
        self.__writer = None

    def setup(self, sample_based_features_names, **kwargs):
        try:
            self.__file = open(self.__filename, 'w')
            self.__writer = csv.writer(self.__file)
            self.__writer.writerow(sample_based_features_names)
        except Exception as e:
            print(e.__class__, str(e))

    def tear_down(self):
        if self.__file:
            self.__file.close()

    def send(self, data, logger):
        try:
            self.__writer.writerow(data)
        except Exception as e:
            if logger:
                logger.error("failed writing data to file. exception: {0}".format(str(e)))
            else:
                print(e.__class__, str(e))

    def setup_output_for_file_based_features(self, file_based_features_names, **kwargs):
        try:
            self.__writer.writerow(file_based_features_names)
        except Exception as e:
            print(e.__class__, str(e))

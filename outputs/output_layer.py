from abc import ABC, abstractmethod


class OutputLayer(ABC):

    def __init__(self, output_format):
        self.output_format = output_format

    @abstractmethod
    def setup(self, file_based_features_names, sample_based_features_names):
        pass

    @abstractmethod
    def send(self, data, logger):
        pass

    @abstractmethod
    def setup_output_for_file_based_features(self, file_based_features_names):
        pass

    @abstractmethod
    def tear_down(self):
        pass

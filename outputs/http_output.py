import requests
import json
from outputs.output_layer import OutputLayer


class HttpOutput(OutputLayer):

    def __init__(self, **kwargs):
        super().__init__("dict")
        self.__url = kwargs["url"]
        self.__headers = kwargs["headers"]

    def setup(self, **kargs):
        pass

    def send(self, data, logger=None):
        try:
            res = requests.post(self.__url, headers=self.__headers, json=data)
            if res.status_code != 200:
                if logger:
                    logger.error("got bad status code ({0}) from server".format(res.status_code))
                else:
                    print("got bad status code ({0}) from server".format(res.status_code))
                    print(json.dumps(data))
        except Exception as e:
            if logger:
                logger.error("exception while sending data to server. exception: {0}".format(str(e)))
            else:
                print("exception while sending data to server. exception: {0}".format(str(e)))

    def setup_output_for_file_based_features(self, **args):
        pass

    def tear_down(self):
        pass

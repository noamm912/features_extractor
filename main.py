import argparse
import json
from features_extractors.midi_feaures_extractor import MidiFeaturesExtractor
from director import Director
from outputs.csv_output import CsvOutput
from outputs.http_output import HttpOutput


DEFAULT_MIDI_FILE_PATH = 'midi_files/simple.mid'
DEFAULT_X_VALUE = 3
DEFAULT_Y_VALUE = 3
DEFAULT_C_VALUE = 0
DEFAULT_T_VALUE = 1.5  # seconds
DEFAULT_OUTPUT_LAYER = "csv"


def parse_config_file(path):
    with open(path) as f:
        return json.loads(f.read())


def parse_arguments():
    arg_parser = argparse.ArgumentParser(description='Features extractor')
    arg_parser.add_argument('-f', type=str, help='Path to midi file', default=DEFAULT_MIDI_FILE_PATH)
    arg_parser.add_argument('-x', type=int, help='Get additional features for last X notes', default=DEFAULT_X_VALUE)
    arg_parser.add_argument('-y', type=int,
                            help='Get additional features for Y successive notes (relevant to clusters features)',
                            default=DEFAULT_Y_VALUE)
    arg_parser.add_argument('-c', type=int, help='Channel to process (relevant only if there exist more than one)',
                            default=DEFAULT_C_VALUE)
    arg_parser.add_argument('-t', type=float, help='Max time difference to consider (relevant to rhythm features)',
                            default=DEFAULT_T_VALUE)
    arg_parser.add_argument('-o', type=str, choices=["http", "csv"], default=DEFAULT_OUTPUT_LAYER,
                            help='Output type, default is {0}'.format(DEFAULT_OUTPUT_LAYER))
    return arg_parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()
    print("Extracting features for file: {0}.\n X: {1}\n Y: {2}\n C: {3}\n T: {4}".format(args.f, args.x, args.y, args.c, args.t))
    config = parse_config_file("config.json")

    builder = MidiFeaturesExtractor(args.f, args.x, args.y, args.c, args.t, display_all_columns=True)

    if args.o == "csv":
        output = CsvOutput(**config["outputs"]["csv"])
    else:
        output = HttpOutput(**config["outputs"]["http"])

    director = Director(output, builder)
    director.extract_features()
